package org.nrg.dicom

import org.nrg.dicom.generators.*

class GeneratorRegistry {

    public static final List<DicomSampleGenerator> GENERATORS = [
            new StereometricRelationshipGenerator(),
            new ContentAssessmentResultsGenerator(),
            new OphthalmicMeasurementsGenerator(),
            new BasicStructuredDisplayGenerator(),
            new WaveformGenerator(),
            new RTGenerator()
    ]

    static DicomSampleGenerator byId(String id) {
        GENERATORS.find { it.sampleId() == id }
    }

}
