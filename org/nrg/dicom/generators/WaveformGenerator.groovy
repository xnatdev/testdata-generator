package org.nrg.dicom.generators

import org.dcm4che3.data.Attributes
import org.dcm4che3.data.UID
import org.nrg.dicom.DicomSampleGenerator
import org.nrg.dicom.Utils
import org.nrg.dicom.process.IODModule

import static org.dcm4che3.data.Tag.*
import static org.dcm4che3.data.VR.*

class WaveformGenerator extends DicomSampleGenerator {

    @Override
    String sampleId() {
        'WAVE'
    }

    @Override
    void process(String[] argList) {
        final Attributes ambulatoryECG = newStudyAndInstance('ECG', UID.AmbulatoryECGWaveformStorage)
        ambulatoryECG.setString(BodyPartExamined, CS, 'HEART')
        addSampleData(ambulatoryECG, IODModule.WAVEFORM_IDENTIFICATION)
        ambulatoryECG.newSequence(AcquisitionContextSequence, 0)
        final Attributes waveformItem = new Attributes()
        waveformItem.setString(WaveformOriginality, CS, 'ORIGINAL')
        waveformItem.setInt(NumberOfWaveformChannels, US, 1)
        waveformItem.setInt(NumberOfWaveformSamples, UL, 16)
        waveformItem.setString(SamplingFrequency, DS, '300')
        final Attributes channelDefinitionItem = new Attributes()
        channelDefinitionItem.setString(ChannelStatus, CS, 'INVALID')
        channelDefinitionItem.newSequence(ChannelSourceSequence, 1) << createBasicCodedEntry('2:105', 'MDC', 'Chest-manubrium lead')
        channelDefinitionItem.setString(ChannelTimeSkew, DS, '0')
        channelDefinitionItem.setInt(WaveformBitsStored, US, 8)
        waveformItem.newSequence(ChannelDefinitionSequence, 1) << channelDefinitionItem
        waveformItem.setInt(WaveformBitsAllocated, US, 8)
        waveformItem.setString(WaveformSampleInterpretation, CS, 'SB')
        waveformItem.setBytes(WaveformData, OB, 'I HAVE NO IDEA WHAT TO ENCODE IN HERE'.bytes)
        ambulatoryECG.newSequence(WaveformSequence, 1) << waveformItem
        Utils.writeDicomToFile(ambulatoryECG, 'ambulatory_ECG')
    }

}
