package org.nrg.dicom.generators

import org.dcm4che3.data.Attributes
import org.dcm4che3.data.UID
import org.nrg.dicom.DicomSampleGenerator
import org.nrg.dicom.Utils
import org.nrg.dicom.process.IODModule
import org.nrg.dicom.process.SOPInstanceReference

import static org.dcm4che3.data.Tag.*
import static org.dcm4che3.data.VR.*

class BasicStructuredDisplayGenerator extends DicomSampleGenerator {

    private final float zero = 0.0 as float
    private final float one = 1.0 as float

    @Override
    String sampleId() {
        'BSD'
    }

    @Override
    void process(String[] argList) {
        final Attributes baseImage = baseImageFromDefaultInterface(argList)
        final Attributes basicStructuredDisplay = newBaseInstance('PR', UID.BasicStructuredDisplayStorage)
        addSampleData(basicStructuredDisplay, IODModule.ENHANCED_GENERAL_EQUIPMENT)
        copyDataInModules(baseImage, basicStructuredDisplay, IODModule.PATIENT, IODModule.GENERAL_STUDY, IODModule.PATIENT_STUDY)
        basicStructuredDisplay.addSelected(baseImage, BodyPartExamined)
        basicStructuredDisplay.setString(SeriesDescription, LO, 'Basic Structured Display')

        // PS3.3 C.11.16 Structured Display Module
        addContentIdentification(basicStructuredDisplay, 'STRUCT_DISPLAY', 'Basic CR Structured Display')
        basicStructuredDisplay.setDate(PresentationCreationDate, DA, now)
        basicStructuredDisplay.setDate(PresentationCreationTime, TM, now)
        basicStructuredDisplay.setInt(NumberOfScreens, US, 1)
        final Attributes nominalScreenDefinitionItem = new Attributes()
        nominalScreenDefinitionItem.setInt(NumberOfVerticalPixels, US, baseImage.getInt(Rows, 0))
        nominalScreenDefinitionItem.setInt(NumberOfHorizontalPixels, US, baseImage.getInt(Columns, 0))
        nominalScreenDefinitionItem.setFloat(DisplayEnvironmentSpatialPosition, FD, zero, one, one, zero)
        nominalScreenDefinitionItem.setInt(ScreenMinimumGrayscaleBitDepth, US, baseImage.getInt(BitsAllocated, 16))
        basicStructuredDisplay.newSequence(NominalScreenDefinitionSequence, 1) << nominalScreenDefinitionItem

        // PS3.3 C.11.17 Structured Display Image Box Module
        final Attributes imageBoxItem = new Attributes()
        imageBoxItem.setFloat(DisplayEnvironmentSpatialPosition, FD, zero, one, one, zero)
        imageBoxItem.setInt(ImageBoxNumber, US, 1)
        imageBoxItem.setString(ImageBoxLayoutType, CS, 'SINGLE')
        imageBoxItem.newSequence(ReferencedImageSequence, 1) << SOPInstanceReference.build(baseImage)
        basicStructuredDisplay.newSequence(StructuredDisplayImageBoxSequence, 1) << imageBoxItem

        // PS3.3 C.11.18 Structured Display Annotation Module
        final Attributes textBoxItem = new Attributes()
        textBoxItem.setString(UnformattedTextValue, ST, 'Sample Structured Display annotation')
        textBoxItem.setFloat(DisplayEnvironmentSpatialPosition, FD, zero, one, 0.1 as float, 0.7 as float)
        textBoxItem.setString(BoundingBoxTextHorizontalJustification, CS, 'CENTER')
        basicStructuredDisplay.newSequence(StructuredDisplayTextBoxSequence, 1) << textBoxItem

        addReferencedSeriesSequence(basicStructuredDisplay, baseImage)

        Utils.writeDicomToFile(basicStructuredDisplay, 'basic_structured_display')
    }

}
