package org.nrg.dicom.generators

import org.dcm4che3.data.*
import org.nrg.dicom.DicomSampleGenerator
import org.nrg.dicom.Utils
import org.nrg.dicom.process.IODModule
import org.nrg.dicom.process.SOPInstanceReference

import static org.dcm4che3.data.Tag.*
import static org.dcm4che3.data.VR.*

class ContentAssessmentResultsGenerator extends DicomSampleGenerator {

    @Override
    String sampleId() {
        'ASMT'
    }

    @Override
    void process(String[] argList) {
        final CliBuilder cli = new CliBuilder()
        cli.with {
            i(longOpt: 'image', 'Image instance from which to build assessment.', required: true, args: 1)
            w(longOpt: 'warnings', 'File containing a list of warnings and errors from dciodvfy to encode into a Content Assessment Results instance.', required: true, args: 1)
        }
        final OptionAccessor params = cli.parse(argList)
        final Attributes baseImage = Utils.readDicomFrom(new File("processing/${params.image}"))
        final List<String> complaints = new File("processing/${params.w}").readLines()
        final List<String> errors = complaints.findAll { it.startsWith('Error -') }
        final List<String> warnings = complaints.findAll { it.startsWith('Warning -') }

        final Attributes dataset = newBaseInstance('ASMT', UID.ContentAssessmentResultsStorage)
        addSampleData(dataset, IODModule.ENHANCED_GENERAL_EQUIPMENT)
        copyDataInModules(baseImage, dataset, IODModule.PATIENT, IODModule.GENERAL_STUDY, IODModule.PATIENT_STUDY)
        dataset.setString(SeriesDescription, LO, 'Partial dciodvfy output')

        addReferencedSeriesSequence(dataset, baseImage)

        // PS3.3 C.33.1 Content Assessment Results Module
        dataset.setString(AssessmentLabel, LO, 'dciodvfy Results')
        dataset.newSequence(AssessmentTypeCodeSequence, 1) << createBasicCodedEntry('151289', '99ASMT', 'Compliance Assessment')
        dataset.newSequence(AssessmentRequesterSequence, 0)
        dataset.newSequence(AssessedSOPInstanceSequence, 1) << SOPInstanceReference.build(baseImage)
        dataset.setString(AssessmentSummary, CS, errors.isEmpty() ? 'PASSED' : 'FAILED')
        dataset.setString(AssessmentSummaryDescription, UT, "A subset of the output of dciodvfy lists ${errors.size()} errors and ${warnings.size()} warnings")
        dataset.setInt(NumberOfAssessmentObservations, UL, errors.size() + warnings.size())

        final Sequence assessmentObservationsSequence = dataset.newSequence(AssessmentObservationsSequence, errors.size() + warnings.size())
        errors.each { error ->
            assessmentObservationsSequence << createObservation(error, 'MAJOR')
        }
        warnings.each { warning ->
            assessmentObservationsSequence << createObservation(warning, 'MINOR')
        }

        Utils.writeDicomToFile(dataset, new File("processing/content_asmt.dcm"))
        Utils.writeDicomToFile(dataset, 'content_asmt')
    }

    private Attributes createObservation(String observationString, String significance) {
        final Attributes observation = new Attributes()

        observation.setString(ObservationSignificance, CS, significance)
        observation.newSequence(ObservationBasisCodeSequence, 1) << createBasicCodedEntry('121376', 'DCM', 'Assessment By Rules')
        observation.setString(ObservationDescription, UT, observationString)
        observation.newSequence(StructuredConstraintObservationSequence, 0)

        observation
    }

}
