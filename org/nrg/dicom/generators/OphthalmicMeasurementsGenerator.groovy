package org.nrg.dicom.generators

import org.dcm4che3.data.Attributes
import org.nrg.dicom.DicomSampleGenerator
import org.nrg.dicom.Utils
import org.nrg.dicom.process.IODModule
import org.nrg.dicom.process.SOPInstanceReference

import static org.dcm4che3.data.Tag.*
import static org.dcm4che3.data.UID.*
import static org.dcm4che3.data.VR.*
import static org.nrg.dicom.Utils.randomFloat

@SuppressWarnings('GrMethodMayBeStatic')
class OphthalmicMeasurementsGenerator extends DicomSampleGenerator {

    @Override
    String sampleId() {
        'OPM'
    }

    @Override
    void process(String[] argList) {
        final Attributes baseImage = baseImageFromDefaultInterface(argList)

        final Attributes lensometryMeasurements = baseOphthalmicRefractiveMeasurementsInstance(baseImage, 'LEN', LensometryMeasurementsStorage)
        lensometryMeasurements.setString(SeriesDescription, LO, 'Patient Glasses Measurements')
        lensometryMeasurements.setString(LensDescription, LO, 'MXIEP 99GLS-8N glasses belonging to the patient, both lenses')
        lensometryMeasurements.newSequence(LeftLensSequence, 1) << createLensometryMeasurementsSequenceItem()
        lensometryMeasurements.newSequence(RightLensSequence, 1) << createLensometryMeasurementsSequenceItem()

        final Attributes autorefractionMeasurements = baseOphthalmicRefractiveMeasurementsInstance(baseImage, 'AR', AutorefractionMeasurementsStorage)
        autorefractionMeasurements.setString(SeriesDescription, LO, 'Autorefraction Measurements')
        autorefractionMeasurements.newSequence(AutorefractionRightEyeSequence, 1) << createAutorefractionSequenceItem()
        autorefractionMeasurements.newSequence(AutorefractionLeftEyeSequence, 1) << createAutorefractionSequenceItem()
        final float nearPD = randomFloat(54.0, 70.0)
        final float distancePD = nearPD + 3.0 as float
        autorefractionMeasurements.setFloat(DistancePupillaryDistance, FD, distancePD)
        autorefractionMeasurements.setFloat(NearPupillaryDistance, FD, nearPD)

        final Attributes keratometryMeasurements = baseOphthalmicRefractiveMeasurementsInstance(baseImage, 'KER', KeratometryMeasurementsStorage)
        keratometryMeasurements.setString(SeriesDescription, LO, 'Keratometry Measurements')
        keratometryMeasurements.newSequence(KeratometryRightEyeSequence, 1) << createKeratometrySequenceItem()
        keratometryMeasurements.newSequence(KeratometryLeftEyeSequence, 1) << createKeratometrySequenceItem()

        final Attributes subjectiveRefractionMeasurements = baseOphthalmicRefractiveMeasurementsInstance(baseImage, 'SRF', SubjectiveRefractionMeasurementsStorage)
        subjectiveRefractionMeasurements.setString(SeriesDescription, LO, 'Subjective Refraction Measurements')
        subjectiveRefractionMeasurements.newSequence(SubjectiveRefractionRightEyeSequence, 1) << createSubjectiveRefractionMeasurementsItem()
        subjectiveRefractionMeasurements.newSequence(SubjectiveRefractionLeftEyeSequence, 1) << createSubjectiveRefractionMeasurementsItem()
        subjectiveRefractionMeasurements.setFloat(DistancePupillaryDistance, FD, distancePD)
        subjectiveRefractionMeasurements.setFloat(NearPupillaryDistance, FD, nearPD)

        final Attributes visualAcuityMeasurements = baseOphthalmicRefractiveMeasurementsInstance(baseImage, 'VA', VisualAcuityMeasurementsStorage)
        visualAcuityMeasurements.setString(SeriesDescription, LO, 'Visual Acuity Measurements')
        visualAcuityMeasurements.newSequence(ReferencedRefractiveMeasurementsSequence, 1) << SOPInstanceReference.build(lensometryMeasurements)
        visualAcuityMeasurements.setString(ViewingDistanceType, CS, 'NEAR')
        visualAcuityMeasurements.newSequence(VisualAcuityTypeCodeSequence, 1) << createBasicCodedEntry('111686', 'DCM', 'Habitual Visual Acuity')
        visualAcuityMeasurements.setString(BackgroundColor, CS, 'GREEN')
        visualAcuityMeasurements.setString(Optotype, CS, 'LETTERS')
        visualAcuityMeasurements.setString(OptotypeDetailedDefinition, LO, 'LogMAR chart')
        visualAcuityMeasurements.setString(OptotypePresentation, CS, 'SINGLE')
        visualAcuityMeasurements.newSequence(VisualAcuityRightEyeSequence, 1) << createVisualAcuityMeasurementsItem()
        visualAcuityMeasurements.newSequence(VisualAcuityLeftEyeSequence, 1) << createVisualAcuityMeasurementsItem()

        final Attributes ophthalmicAxialMeasurements = baseOphthalmicRefractiveMeasurementsInstance(baseImage, 'OAM', OphthalmicAxialMeasurementsStorage)
        ophthalmicAxialMeasurements.setString(SeriesDescription, LO, '<Phakic> Ophthalmic Axial Measurements')
        ophthalmicAxialMeasurements.setString(OphthalmicAxialMeasurementsDeviceType, CS, 'OPTICAL')
        ophthalmicAxialMeasurements.newSequence(OphthalmicAxialMeasurementsRightEyeSequence, 1) << createOphthalmicAxialMeasurementsItem()
        ophthalmicAxialMeasurements.newSequence(OphthalmicAxialMeasurementsLeftEyeSequence, 1) << createOphthalmicAxialMeasurementsItem()

        final Attributes intraocularLensCalculations = baseOphthalmicRefractiveMeasurementsInstance(baseImage, 'IOL', IntraocularLensCalculationsStorage)
        intraocularLensCalculations.setString(SeriesDescription, LO, 'Intraocular Lens Calculations')
        intraocularLensCalculations.newSequence(IntraocularLensCalculationsRightEyeSequence, 1) << createIntraocularLensCalculationsSequenceItem()
        intraocularLensCalculations.newSequence(IntraocularLensCalculationsLeftEyeSequence, 1) << createIntraocularLensCalculationsSequenceItem()

        [
                lensometry : lensometryMeasurements,
                autorefraction : autorefractionMeasurements,
                keratometry : keratometryMeasurements,
                subjective_refraction : subjectiveRefractionMeasurements,
                visual_acuity : visualAcuityMeasurements,
                ophthalmic_axial : ophthalmicAxialMeasurements,
                intraocular_lens : intraocularLensCalculations,
                visual_field_perimetry : createFullVisualFieldStaticPerimetryMeasurements(baseImage)
        ].each { name, dataset ->
            Utils.writeDicomToFile(dataset, name)
        }
    }

    private Attributes baseOphthalmicRefractiveMeasurementsInstance(Attributes baseImage, String modality, String sopClassUID) {
        final Attributes instance = newBaseInstance(modality, sopClassUID)
        addSampleData(instance, IODModule.ENHANCED_GENERAL_EQUIPMENT, IODModule.GENERAL_OPHTHALMIC_REFRACTIVE_MEASUREMENTS)
        copyDataInModules(baseImage, instance, IODModule.PATIENT, IODModule.GENERAL_STUDY, IODModule.PATIENT_STUDY)
        instance.setString(MeasurementLaterality, CS, 'B')
        instance.setString(ImageComments, LT, 'This SOP instance created for testing purposes only')
        instance
    }

    private Attributes createLensometryMeasurementsSequenceItem() {
        final Attributes attributes = new Attributes()

        attributes.setFloat(SpherePower, FD, randomFloat(0.0, 1.3))
        addCylinderSequence(attributes, randomFloat(0.5, 1.5), Utils.randomInteger(50))
        attributes.newSequence(AddNearSequence, 1) << createDistanceSequence(10)
        attributes.newSequence(AddIntermediateSequence, 1) << createDistanceSequence(500)
        addPrismSequence(attributes, 0.3 as float, 'OUT', 0.4 as float, 'DOWN')
        attributes.setString(LensSegmentType, CS, 'PROGRESSIVE')
        attributes.setFloat(OpticalTransmittance, FD, randomFloat(70, 95))

        attributes
    }

    private Attributes createDistanceSequence(int viewingDistance) {
        final Attributes sequenceItem = new Attributes()
        sequenceItem.setFloat(AddPower, FD, randomFloat(0.0, 0.5))
        sequenceItem.setFloat(ViewingDistance, FD, viewingDistance as float)
        sequenceItem
    }

    private Attributes createAutorefractionSequenceItem() {
        final Attributes sequenceItem = new Attributes()
        sequenceItem.setFloat(SpherePower, FD, randomFloat(0.0, 1.3))
        sequenceItem.setFloat(PupilSize, FD, randomFloat(2.0, 4.5))
        sequenceItem.setFloat(CornealSize, FD, randomFloat(11.0, 12.0))
        sequenceItem
    }

    private Attributes createKeratometrySequenceItem() {
        final float radiusOfCurvature = randomFloat(6.5, 7.5)
        final float keratometricPower = randomFloat(42.0, 46.0)
        final float keratometricAxis = randomFloat(14.0, 18.0)
        final Attributes keratometrySequenceItem = new Attributes()
        keratometrySequenceItem.newSequence(SteepKeratometricAxisSequence, 1) << createKeratometricAxisSequenceItem(radiusOfCurvature, keratometricPower, keratometricAxis)
        keratometrySequenceItem.newSequence(FlatKeratometricAxisSequence, 1) << createKeratometricAxisSequenceItem(radiusOfCurvature, keratometricPower, keratometricAxis)

        keratometrySequenceItem
    }

    private Attributes createKeratometricAxisSequenceItem(float radiusOfCurvature, float keratometricPower, float keratometricAxis) {
        final Attributes sequenceItem = new Attributes()
        sequenceItem.setFloat(RadiusOfCurvature, FD, radiusOfCurvature)
        sequenceItem.setFloat(KeratometricPower, FD, keratometricPower)
        sequenceItem.setFloat(KeratometricAxis, FD, keratometricAxis)
        sequenceItem
    }

    private Attributes createSubjectiveRefractionMeasurementsItem() {
        final Attributes sequenceItem = new Attributes()
        sequenceItem.setFloat(SpherePower, FD, randomFloat(0.0, 1.3))
        sequenceItem.newSequence(AddNearSequence, 1) << createDistanceSequence(10)
        sequenceItem
    }

    private Attributes createVisualAcuityMeasurementsItem() {
        final Attributes sequenceItem = new Attributes()
        sequenceItem.setFloat(DecimalVisualAcuity, FD, randomFloat(0.3, 0.8))
        sequenceItem
    }

    private Attributes createOphthalmicAxialMeasurementsItem() {
        final float axialLength = randomFloat(12.0, 16.0)
        Attributes sequenceItem = new Attributes()
        sequenceItem.newSequence(LensStatusCodeSequence, 1) << createBasicCodedEntry('R-2073F', 'SRT', 'Phakic')
        sequenceItem.newSequence(VitreousStatusCodeSequence, 1) << createBasicCodedEntry('T-AA092', 'SRT', 'Vitreous Only')
        sequenceItem.setString(PupilDilated, CS, 'NO')

        final Attributes axialLengthMeasurementsItem = new Attributes()
        axialLengthMeasurementsItem.setString(OphthalmicAxialLengthMeasurementsType, CS, 'SEGMENTAL LENGTH')
        final Attributes segmentalLengthSequenceItem = new Attributes()
        segmentalLengthSequenceItem.setFloat(OphthalmicAxialLength, FL, axialLength)
        segmentalLengthSequenceItem.setString(OphthalmicAxialLengthMeasurementModified, CS, 'NO')
        segmentalLengthSequenceItem.newSequence(OphthalmicAxialLengthMeasurementsSegmentNameCodeSequence, 1) << createBasicCodedEntry('111779', 'DCM', 'Posterior Lens')
        final Attributes opticalAxialLengthSequenceItem = new Attributes()

        opticalAxialLengthSequenceItem.newSequence(OphthalmicAxialLengthDataSourceCodeSequence, 1) << createBasicCodedEntry('113857', 'DCM', 'Manual Entry')
        segmentalLengthSequenceItem.newSequence(OpticalOphthalmicAxialLengthMeasurementsSequence, 1) << opticalAxialLengthSequenceItem
        axialLengthMeasurementsItem.newSequence(OphthalmicAxialLengthMeasurementsSegmentalLengthSequence, 1) << segmentalLengthSequenceItem
        sequenceItem.newSequence(OphthalmicAxialLengthMeasurementsSequence, 1) << axialLengthMeasurementsItem

        sequenceItem.newSequence(OpticalSelectedOphthalmicAxialLengthSequence, 1) << new Attributes()

        sequenceItem
    }

    private Attributes createIntraocularLensCalculationsSequenceItem() {
        final Attributes intraocularLensItem = new Attributes()

        intraocularLensItem.setFloat(TargetRefraction, FL, randomFloat(0.2, 1.0))
        intraocularLensItem.setString(RefractiveProcedureOccurred, CS, 'NO')
        intraocularLensItem.newSequence(RefractiveStateSequence, 0)
        intraocularLensItem.addAll(createKeratometrySequenceItem())
        intraocularLensItem.newSequence(KeratometryMeasurementTypeCodeSequence, 1) << createBasicCodedEntry('111753', 'DCM', 'Manual Keratometry')
        intraocularLensItem.setFloat(KeratometerIndex, FL, 1.3375 as float)
        intraocularLensItem.newSequence(IOLFormulaCodeSequence, 1) << createBasicCodedEntry('111761', 'DCM', 'Haigis-L')

        final Attributes ophthalmicAxialLengthItem = new Attributes()
        ophthalmicAxialLengthItem.setFloat(OphthalmicAxialLength, FL, randomFloat(22.0, 25.0))
        ophthalmicAxialLengthItem.newSequence(OphthalmicAxialLengthSelectionMethodCodeSequence, 1) << createBasicCodedEntry('121410', 'DCM', 'User chosen value')
        ophthalmicAxialLengthItem.newSequence(SourceOfOphthalmicAxialLengthCodeSequence, 1) << createBasicCodedEntry('111781', 'DCM', 'External Data Source')
        intraocularLensItem.newSequence(OphthalmicAxialLengthSequence, 1) << ophthalmicAxialLengthItem

        intraocularLensItem.setString(IOLManufacturer, LO, 'XNATLM9')
        intraocularLensItem.setString(ImplantName, LO, 'XNATLM9-L400')

        final Attributes lensConstantItem = new Attributes()
        lensConstantItem.newSequence(ConceptNameCodeSequence, 1) << createBasicCodedEntry('111771', 'DCM', 'Haigis a2')
        lensConstantItem.setFloat(NumericValue, DS, randomFloat(0.0, 1.0))
        intraocularLensItem.newSequence(LensConstantSequence, 1) << lensConstantItem

        final Attributes powerSequenceItem = new Attributes()
        powerSequenceItem.setFloat(IOLPower, FL, randomFloat(0.5, 1.5))
        powerSequenceItem.setFloat(PredictedRefractiveError, FL, randomFloat(0.5, 1.5))
        powerSequenceItem.setString(ImplantPartNumber, LO, '')
        intraocularLensItem.newSequence(IOLPowerSequence, 1) << powerSequenceItem

        intraocularLensItem.setNull(IOLPowerForExactEmmetropia, FL)
        intraocularLensItem.setNull(IOLPowerForExactTargetRefraction, FL)

        intraocularLensItem
    }

    private Attributes createFullVisualFieldStaticPerimetryMeasurements(Attributes baseImage) {
        final Attributes visualFieldStaticPerimetryMeasurements = newBaseInstance('OPV', OphthalmicVisualFieldStaticPerimetryMeasurementsStorage)
        addSampleData(visualFieldStaticPerimetryMeasurements, IODModule.ENHANCED_GENERAL_EQUIPMENT)
        copyDataInModules(baseImage, visualFieldStaticPerimetryMeasurements, IODModule.PATIENT, IODModule.GENERAL_STUDY, IODModule.PATIENT_STUDY)
        visualFieldStaticPerimetryMeasurements.setString(SeriesDescription, LO, 'Full Field 120 Point Perimetry Measurements')

        // PS3.3 C.8.26.1 Visual Field Static Perimetry Measurements Series Module
        visualFieldStaticPerimetryMeasurements.setString(PerformedProcedureStepID, SH, '999PSI')
        final Attributes testPatternsItem = createBasicCodedEntry('111809', 'DCM', 'Visual Field Full Field 120 Point Test Pattern')
        testPatternsItem.newSequence(ProtocolContextSequence, 1) << createVisualFieldContentItem()
        final Attributes testStrategiesItem = createBasicCodedEntry('111821', 'DCM', 'Visual Field Optima Test Strategy')
        testStrategiesItem.newSequence(ProtocolContextSequence, 1) << createVisualFieldContentItem()
        visualFieldStaticPerimetryMeasurements.newSequence(PerformedProtocolCodeSequence, 2).addAll([testPatternsItem, testStrategiesItem])

        // PS3.3 C.8.26.2 Visual Field Static Perimetry Test Parameters Module
        visualFieldStaticPerimetryMeasurements.setFloat(VisualFieldHorizontalExtent, FL, 45.0 as float)
        visualFieldStaticPerimetryMeasurements.setFloat(VisualFieldVerticalExtent, FL, 45.0 as float)
        visualFieldStaticPerimetryMeasurements.setString(VisualFieldShape, CS, 'CIRCLE')
        visualFieldStaticPerimetryMeasurements.setFloat(MaximumStimulusLuminance, FL, 249.5 as float)
        visualFieldStaticPerimetryMeasurements.setFloat(BackgroundLuminance, FL, 87.0 as float)
        visualFieldStaticPerimetryMeasurements.newSequence(StimulusColorCodeSequence, 1) << createBasicCodedEntry('G-A11A', 'SRT', 'Red')
        visualFieldStaticPerimetryMeasurements.newSequence(BackgroundIlluminationColorCodeSequence, 1) << createBasicCodedEntry('G-A12F', 'SRT', 'Blue')
        visualFieldStaticPerimetryMeasurements.setFloat(StimulusArea, FL, 2.2 as float)
        visualFieldStaticPerimetryMeasurements.setFloat(StimulusPresentationTime, FL, 400.0 as float)

        // PS3.3 C.8.26.3 Visual Field Static Perimetry Test Reliability Module
        final Attributes fixationSequenceItem = new Attributes()
        fixationSequenceItem.newSequence(FixationMonitoringCodeSequence, 1) << createBasicCodedEntry('111843', 'DCM', 'Automated Optical')
        fixationSequenceItem.setString(ExcessiveFixationLossesDataFlag, CS, 'YES')
        fixationSequenceItem.setString(ExcessiveFixationLosses, CS, 'NO')
        visualFieldStaticPerimetryMeasurements.newSequence(FixationSequence, 1) << fixationSequenceItem
        final Attributes visualFieldCatchTrialItem = new Attributes()
        [CatchTrialsDataFlag, FalseNegativesEstimateFlag, ExcessiveFalseNegativesDataFlag, FalsePositivesEstimateFlag, ExcessiveFalsePositivesDataFlag].each { attribute ->
            visualFieldCatchTrialItem.setString(attribute, CS, 'NO')
        }
        visualFieldStaticPerimetryMeasurements.newSequence(VisualFieldCatchTrialSequence, 1) << visualFieldCatchTrialItem

        // PS3.3 C.8.26.4 Visual Field Static Perimetry Test Measurements Module
        visualFieldStaticPerimetryMeasurements.setString(MeasurementLaterality, CS, 'B')
        visualFieldStaticPerimetryMeasurements.setString(PresentedVisualStimuliDataFlag, CS, 'YES')
        visualFieldStaticPerimetryMeasurements.setInt(NumberOfVisualStimuli, US, 322)
        visualFieldStaticPerimetryMeasurements.setFloat(VisualFieldTestDuration, FL, randomFloat(180.0, 210.0))
        visualFieldStaticPerimetryMeasurements.setString(FovealSensitivityMeasured, CS, 'NO')
        visualFieldStaticPerimetryMeasurements.setString(FovealPointNormativeDataFlag, CS, 'NO')
        visualFieldStaticPerimetryMeasurements.setString(ScreeningBaselineMeasured, CS, 'NO')
        visualFieldStaticPerimetryMeasurements.setString(BlindSpotLocalized, CS, 'YES')
        final float blindSpotX = randomFloat(1.0, 5.0)
        final float blindSpotY = randomFloat(1.0, 5.0)
        visualFieldStaticPerimetryMeasurements.setFloat(BlindSpotXCoordinate, FL, blindSpotX)
        visualFieldStaticPerimetryMeasurements.setFloat(BlindSpotYCoordinate, FL, blindSpotY)
        visualFieldStaticPerimetryMeasurements.setFloat(MinimumSensitivityValue, FL, 2.0 as float)
        visualFieldStaticPerimetryMeasurements.setString(TestPointNormalsDataFlag, CS, 'NO')
        final Attributes testPointItem = new Attributes()
        testPointItem.setFloat(VisualFieldTestPointXCoordinate, FL, blindSpotX)
        testPointItem.setFloat(VisualFieldTestPointYCoordinate, FL, blindSpotY)
        testPointItem.setString(StimulusResults, CS, 'NOT SEEN')
        visualFieldStaticPerimetryMeasurements.newSequence(VisualFieldTestPointSequence, 1) << testPointItem

        // PS3.3 C.8.26.5 Visual Field Static Perimetry Test Results Module
        [VisualFieldTestNormalsFlag, ShortTermFluctuationCalculated, ShortTermFluctuationProbabilityCalculated, CorrectedLocalizedDeviationFromNormalCalculated, CorrectedLocalizedDeviationFromNormalProbabilityCalculated].each { attribute ->
            visualFieldStaticPerimetryMeasurements.setString(attribute, CS, 'NO')
        }

        // PS3.3 C.8.26.6 Ophthalmic Patient Clinical Information and Test Lens Parameters Module
        visualFieldStaticPerimetryMeasurements.newSequence(OphthalmicPatientClinicalInformationLeftEyeSequence, 1) << createOphthalmicPatientClinicalItem()
        visualFieldStaticPerimetryMeasurements.newSequence(OphthalmicPatientClinicalInformationRightEyeSequence, 1) << createOphthalmicPatientClinicalItem()

        visualFieldStaticPerimetryMeasurements
    }

    private Attributes createVisualFieldContentItem() {
        final Attributes contentItem = new Attributes()
        contentItem.setString(ValueType, CS, 'CODE')
        contentItem.newSequence(ConceptNameCodeSequence, 1) << createBasicCodedEntry('99-8878', '99XNAT', 'Procedure Modifier')
        contentItem.newSequence(ConceptCodeSequence, 1) << createBasicCodedEntry('R-42453', 'SRT', 'Screening')
        contentItem
    }

    private Attributes createOphthalmicPatientClinicalItem() {
        final Attributes item = new Attributes()
        item.newSequence(RefractiveParametersUsedOnPatientSequence, 0)
        item.setFloat(PupilSize, FD, 5.0 as float)
        item.setString(PupilDilated, CS, 'YES')
        item
    }

    /**
     * PS3.3 C.8.25.6 General Ophthalmic Refractive Macros
     */

    private void addCylinderSequence(Attributes baseObject, float cylinderPower, float cylinderAxis) {
        final Attributes sequenceItem = new Attributes()
        sequenceItem.setFloat(CylinderPower, FD, cylinderPower)
        sequenceItem.setFloat(CylinderAxis, FL, cylinderAxis)
        baseObject.newSequence(CylinderSequence, 1) << sequenceItem
    }

    private void addPrismSequence(Attributes baseObject, float horizontalPrismPower, String horizontalPrismBase, float verticalPrismPower, String verticalPrismBase) {
        final Attributes sequenceItem = new Attributes()
        sequenceItem.setFloat(HorizontalPrismPower, FD, horizontalPrismPower)
        sequenceItem.setString(HorizontalPrismBase, CS, horizontalPrismBase)
        sequenceItem.setFloat(VerticalPrismPower, FD, verticalPrismPower)
        sequenceItem.setString(VerticalPrismBase, CS, verticalPrismBase)
        baseObject.newSequence(PrismSequence, 1) << sequenceItem
    }

}
