package org.nrg.dicom.generators

import org.dcm4che3.data.Attributes
import org.nrg.dicom.DicomSampleGenerator
import org.nrg.dicom.Utils
import org.nrg.dicom.model.Person
import org.nrg.dicom.process.IODModule

import static org.dcm4che3.data.Tag.*
import static org.dcm4che3.data.UID.*
import static org.dcm4che3.data.VR.*

class RTGenerator extends DicomSampleGenerator {

    @Override
    String sampleId() {
        'RT'
    }

    @Override
    void process(String[] args) {
        final CliBuilder cli = new CliBuilder()
        cli.with {
            c(longOpt: 'ct-image', 'CT Image instance from which to build study data.', required: true, args: 1)
        }
        final OptionAccessor params = cli.parse(args)
        final Attributes ctImage = Utils.readDicomFrom(new File("processing/${params.c}"))
        final Person physician = Utils.randomPerson()
        final Attributes physicianIntent = newBaseInstance('RTINTENT', RTPhysicianIntentStorage)
        copyDataInModules(ctImage, physicianIntent, IODModule.PATIENT, IODModule.GENERAL_STUDY, IODModule.PATIENT_STUDY)
        addSampleData(physicianIntent, IODModule.ENHANCED_RT_SERIES, IODModule.ENHANCED_GENERAL_EQUIPMENT)

        // PS3.3 C.36.5 RT Physician Intent Module
        physicianIntent.setString(UserContentLongLabel, LO, 'Sample Physician Intent')
        physicianIntent.setString(ContentDescription, LO, 'Test-only sample RT Physician Intent SOP instance')
        physicianIntent.setString(ContentCreatorName, PN, physician.dicomName())
        physicianIntent.setString(RTTreatmentPhaseIntentPresenceFlag, CS, 'NO')
        final Attributes physicianIntentItem = new Attributes()
        physicianIntentItem.setInt(RTPhysicianIntentIndex, US, 1)
        physicianIntentItem.setString(TreatmentSite, LO, 'NECK')
        physicianIntentItem.newSequence(TreatmentSiteCodeSequence, 1) << createBasicCodedEntry('8M46-P', '99XNAT', 'Neck')
        physicianIntentItem.setString(RTPhysicianIntentNarrative, UT, 'Apply radiotherapy to cure patient')
        physicianIntentItem.setString(RTTreatmentIntentType, CS, 'CURATIVE')
        physicianIntentItem.setString(RTTreatmentApproachLabel, LO, 'TSE')
        physicianIntentItem.newSequence(RTProtocolCodeSequence, 1) << createBasicCodedEntry('7YYYT-9', '99XNAT', '7Y RT')
        physicianIntentItem.newSequence(RTDiagnosisCodeSequence, 1) << createBasicCodedEntry('TMR5-M', '99XNAT', 'Medium tumor')
        physicianIntentItem.newSequence(RTPhysicianIntentInputInstanceSequence, 0)
        physicianIntent.newSequence(RTPhysicianIntentSequence, 1) << physicianIntentItem

        // PS3.3 C.36.4 Radiotherapy Common Instance Module
        physicianIntent.setDate(InstanceCreationDate, DA, now)
        physicianIntent.setDate(InstanceCreationTime, TM, now)
        physicianIntent.setDate(ContentDate, DA, now)
        physicianIntent.setDate(ContentTime, TM, now)
        final Attributes authorIdentificationItem = new Attributes()
        authorIdentificationItem.setString(ObserverType, CS, 'PSN')
        authorIdentificationItem.setString(PersonName, PN, physician.dicomName())
        authorIdentificationItem.newSequence(PersonIdentificationCodeSequence, 0)
        authorIdentificationItem.newSequence(OrganizationalRoleCodeSequence, 1) << createBasicCodedEntry('J-004E8', 'SRT', 'Physician')
        authorIdentificationItem.setString(InstitutionName, LO, '999 Hospital')
        authorIdentificationItem.newSequence(InstitutionCodeSequence, 0)
        physicianIntent.newSequence(AuthorIdentificationSequence, 1) << authorIdentificationItem

        Utils.writeDicomToFile(physicianIntent, 'rt_physician_intent')
    }

}
