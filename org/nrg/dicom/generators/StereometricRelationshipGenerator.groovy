package org.nrg.dicom.generators

import org.dcm4che3.data.Attributes
import org.dcm4che3.data.Tag
import org.dcm4che3.data.UID
import org.dcm4che3.data.VR
import org.nrg.dicom.DicomSampleGenerator
import org.nrg.dicom.Utils
import org.nrg.dicom.process.IODModule
import org.nrg.dicom.process.SOPInstanceReference

class StereometricRelationshipGenerator extends DicomSampleGenerator {

    @Override
    String sampleId() {
        'SMR'
    }

    @Override
    void process(String[] argList) {
        final CliBuilder cli = new CliBuilder()
        cli.with {
            l(longOpt: 'left',  'Left image to use in Stereometric Relationship instance.',  required: true, args: 1)
            r(longOpt: 'right', 'Right image to use in Stereometric Relationship instance.', required: true, args: 1)
        }
        final OptionAccessor params = cli.parse(argList)

        final Attributes leftImageDataset = Utils.readDicomFrom(new File("processing/${params.l}"))
        final Attributes rightImageDataset = Utils.readDicomFrom(new File("processing/${params.r}"))

        if (leftImageDataset.getString(Tag.SOPInstanceUID) == rightImageDataset.getString(Tag.SOPInstanceUID)) {
            throw new UnsupportedOperationException('SOP Instance UIDs must not be the same for the two images in the pair. See PS3.3 C.8.18.2-1.')
        }
        if (leftImageDataset.getInt(Tag.Rows, 0) != rightImageDataset.getInt(Tag.Rows, 1)) {
            throw new UnsupportedOperationException('Images in pair must have the same value for Rows (0028,0010). See PS3.3 C.8.18.2.1.1.')
        }
        if (leftImageDataset.getInt(Tag.Columns, 0) != rightImageDataset.getInt(Tag.Columns, 1)) {
            throw new UnsupportedOperationException('Images in pair must have the same value for Columns (0028,0011). See PS3.3 C.8.18.2.1.1.')
        }

        final Attributes dataset = newBaseInstance('SMR', UID.StereometricRelationshipStorage)
        copyDataInModules(leftImageDataset, dataset, IODModule.PATIENT, IODModule.GENERAL_STUDY, IODModule.PATIENT_STUDY)
        if (leftImageDataset.getString(Tag.BodyPartExamined) == rightImageDataset.getString(Tag.BodyPartExamined)) {
            dataset.setString(Tag.BodyPartExamined, VR.CS, leftImageDataset.getString(Tag.BodyPartExamined))
        }

        // PS3.3 C.12.2 Common Instance Reference Module
        addReferencedSeriesSequence(dataset, leftImageDataset, rightImageDataset)

        // PS3.3 C.8.18.2 Stereometric Relationship Module
        final Attributes stereoPairsItem = new Attributes()
        stereoPairsItem.newSequence(Tag.LeftImageSequence, 1) << SOPInstanceReference.build(leftImageDataset)
        stereoPairsItem.newSequence(Tag.RightImageSequence, 1) << SOPInstanceReference.build(rightImageDataset)
        dataset.newSequence(Tag.StereoPairsSequence, 1) << stereoPairsItem

        Utils.writeDicomToFile(dataset, 'smr')
    }

}
