package org.nrg.dicom.process

import org.dcm4che3.data.Attributes
import org.dcm4che3.util.UIDUtils
import org.nrg.dicom.Utils
import org.nrg.dicom.model.Person

import static org.dcm4che3.data.Tag.*
import static org.dcm4che3.data.UID.*
import static org.dcm4che3.data.VR.*

enum IODModule {

    PATIENT {
        @Override
        int[] getModuleElements() {
            [
                    PatientName,
                    PatientID,
                    PatientBirthDate,
                    PatientBirthDateInAlternativeCalendar,
                    PatientDeathDateInAlternativeCalendar,
                    PatientAlternativeCalendar,
                    PatientSex,
                    ReferencedPatientPhotoSequence,
                    QualityControlSubject,
                    ReferencedPatientSequence,
                    PatientBirthTime,
                    OtherPatientIDsSequence,
                    OtherPatientNames,
                    EthnicGroup,
                    PatientComments,
                    PatientSpeciesDescription,
                    PatientSpeciesCodeSequence,
                    PatientBreedDescription,
                    PatientBreedCodeSequence,
                    BreedRegistrationSequence,
                    StrainDescription,
                    StrainNomenclature,
                    StrainCodeSequence,
                    StrainAdditionalInformation,
                    StrainStockSequence,
                    GeneticModificationsSequence,
                    ResponsiblePerson,
                    ResponsiblePersonRole,
                    ResponsibleOrganization,
                    PatientIdentityRemoved,
                    DeidentificationMethod,
                    DeidentificationMethodCodeSequence
            ] + MacroAttributes.ISSUER_OF_PATIENT_ID_MACRO_ATTRIBUTES + MacroAttributes.PATIENT_GROUP_MACRO_ATTRIBUTES
        }

        @Override
        void addSampleData(Attributes target) {
            final Person patient = Utils.randomPerson()
            target.setString(PatientName, PN, patient.dicomName())
            target.setString(PatientID, LO, Utils.simpleID())
            target.setNull(PatientBirthDate, DA)
            target.setString(PatientSex, CS, patient.gender.dicomEncoding)
        }
    },
    GENERAL_STUDY {
        @Override
        int[] getModuleElements() {
            [
                    StudyInstanceUID,
                    StudyDate,
                    StudyTime,
                    ReferringPhysicianName,
                    ReferringPhysicianIdentificationSequence,
                    ConsultingPhysicianName,
                    ConsultingPhysicianIdentificationSequence,
                    StudyID,
                    AccessionNumber,
                    IssuerOfAccessionNumberSequence,
                    StudyDescription,
                    PhysiciansOfRecord,
                    PhysiciansOfRecordIdentificationSequence,
                    NameOfPhysiciansReadingStudy,
                    PhysiciansReadingStudyIdentificationSequence,
                    RequestingServiceCodeSequence,
                    ReferencedStudySequence,
                    ProcedureCodeSequence,
                    ReasonForPerformedProcedureCodeSequence
            ]
        }

        @Override
        void addSampleData(Attributes target) {
            target.setString(StudyInstanceUID, UI, UIDUtils.createUID())
            target.setDate(StudyDate, DA, CURRENT_DATE_TIME)
            target.setDate(StudyTime, TM, CURRENT_DATE_TIME)
            target.setString(ReferringPhysicianName, PN, Utils.randomName())
            target.setString(StudyID, SH, Utils.simpleID())
            target.setString(AccessionNumber, SH, Utils.simpleID())
            target.setString(StudyDescription, LO, 'Test Data')
        }
    },
    PATIENT_STUDY {
        @Override
        int[] getModuleElements() {
            [
                    AdmittingDiagnosesDescription,
                    AdmittingDiagnosesCodeSequence,
                    PatientAge,
                    PatientSize,
                    PatientWeight,
                    PatientBodyMassIndex,
                    MeasuredAPDimension,
                    MeasuredLateralDimension,
                    PatientSizeCodeSequence,
                    MedicalAlerts,
                    Allergies,
                    SmokingStatus,
                    PregnancyStatus,
                    LastMenstrualDate,
                    PatientState,
                    Occupation,
                    AdditionalPatientHistory,
                    AdmissionID,
                    IssuerOfAdmissionIDSequence,
                    ServiceEpisodeID,
                    IssuerOfServiceEpisodeIDSequence,
                    ServiceEpisodeDescription,
                    PatientSexNeutered
            ]
        }

        @Override
        void addSampleData(Attributes target) {}
    },
    GENERAL_SERIES {
        @Override
        void addSampleData(Attributes target) {
            target.setString(SeriesInstanceUID, UI, UIDUtils.createUID())
            target.setInt(SeriesNumber, IS, Utils.randomInteger())
            target.setDate(SeriesDate, DA, CURRENT_DATE_TIME)
            target.setDate(SeriesTime, TM, CURRENT_DATE_TIME)
        }
    },
    SR_DOCUMENT_SERIES {
        @Override
        void addSampleData(Attributes target) {
            target.setString(Modality, CS, 'SR')
            target.setString(SeriesInstanceUID, UI, UIDUtils.createUID())
            target.setInt(SeriesNumber, IS, Utils.randomInteger())
            target.setDate(SeriesDate, DA, CURRENT_DATE_TIME)
            target.setDate(SeriesTime, TM, CURRENT_DATE_TIME)
            target.setString(SeriesDescription, LO, 'Structured Report')
            target.newSequence(ReferencedPerformedProcedureStepSequence, 0)
        }
    },
    ENHANCED_RT_SERIES {
        @Override
        void addSampleData(Attributes target) {
            target.setInt(SeriesNumber, IS, Utils.randomInteger())
            target.setDate(SeriesDate, DA, CURRENT_DATE_TIME)
            target.setDate(SeriesTime, TM, CURRENT_DATE_TIME)
            final Attributes nonexistentProcedureStep = new Attributes()
            nonexistentProcedureStep.setString(SOPClassUID, UI, ModalityPerformedProcedureStepSOPClass)
            nonexistentProcedureStep.setString(SOPInstanceUID, UI, UIDUtils.createUID())
            target.newSequence(ReferencedPerformedProcedureStepSequence, 1) << SOPInstanceReference.build(nonexistentProcedureStep)
        }
    },
    GENERAL_EQUIPMENT {
        @Override
        void addSampleData(Attributes target) {
            target.setString(Manufacturer, LO, MANUFACTURER)
            target.setString(ManufacturerModelName, LO, SOURCE)
        }
    },
    ENHANCED_GENERAL_EQUIPMENT {
        @Override
        void addSampleData(Attributes target) {
            GENERAL_EQUIPMENT.addSampleData(target)
            target.setString(DeviceSerialNumber, LO, '999XNAT-1-2-3-4-5')
            target.setString(SoftwareVersions, LO, 'Beta')
        }
    },
    SOP_COMMON {
        @Override
        void addSampleData(Attributes target) {
            target.setString(SOPInstanceUID, UI, UIDUtils.createUID())
            target.setDate(InstanceCreationDate, DA, CURRENT_DATE_TIME)
            target.setDate(InstanceCreationTime, TM, CURRENT_DATE_TIME)
            target.setInt(InstanceNumber, IS, 1)
        }
    },
    GENERAL_OPHTHALMIC_REFRACTIVE_MEASUREMENTS {
        // MeasurementLaterality and ReferencedRefractiveMeasurementsSequence not set
        @Override
        void addSampleData(Attributes target) {
            target.setInt(InstanceNumber, IS, 1)
            target.setDate(ContentDate, DA, CURRENT_DATE_TIME)
            target.setDate(ContentTime, TM, CURRENT_DATE_TIME)
        }
    },
    SR_DOCUMENT_GENERAL {
        @Override
        void addSampleData(Attributes target) {
            target.setInt(InstanceNumber, IS, 1)
            target.setString(PreliminaryFlag, CS, 'FINAL')
            target.setString(CompletionFlag, CS, 'COMPLETE')
            target.setString(VerificationFlag, CS, 'UNVERIFIED')
            target.setDate(ContentDate, DA, CURRENT_DATE_TIME)
            target.setDate(ContentTime, TM, CURRENT_DATE_TIME)
            target.newSequence(PerformedProcedureCodeSequence, 0)
        }
    },
    WAVEFORM_IDENTIFICATION {
        @Override
        void addSampleData(Attributes target) {
            target.setInt(InstanceNumber, IS, 1)
            target.setDate(ContentDate, DA, CURRENT_DATE_TIME)
            target.setDate(ContentTime, TM, CURRENT_DATE_TIME)
            target.setDate(AcquisitionDateTime, DT, CURRENT_DATE_TIME)
        }
    }

    private static final Date CURRENT_DATE_TIME = new Date()
    private static final String MANUFACTURER = 'XNAT'
    private static final String SOURCE = 'https://bitbucket.org/xnatdev/testdata-generator'
    int[] moduleElements

    void copyDataFrom(Attributes source, Attributes target) {
        moduleElements = getModuleElements()
        if (moduleElements == null || moduleElements.length == 0) {
            throw new UnsupportedOperationException('Duplicating data from source Attributes not supported for this IODModule.')
        }
        Arrays.sort(moduleElements)
        target.addSelected(source, moduleElements)
    }

    abstract void addSampleData(Attributes target)

}
