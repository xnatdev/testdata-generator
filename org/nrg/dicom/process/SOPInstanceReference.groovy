package org.nrg.dicom.process

import org.dcm4che3.data.Attributes
import org.dcm4che3.data.Tag
import org.dcm4che3.data.VR

/**
 * Creates a DICOM object with elements from PS3.3 10.8 "SOP Instance Reference Macro" for use in a sequence
 */
class SOPInstanceReference {

    // Can't extend Attributes since it has a method that confuses groovy in compilation
    static Attributes build(Attributes referencedSOPInstance) {
        final Attributes attributes = new Attributes()
        attributes.setString(Tag.ReferencedSOPClassUID, VR.UI, referencedSOPInstance.getString(Tag.SOPClassUID))
        attributes.setString(Tag.ReferencedSOPInstanceUID, VR.UI, referencedSOPInstance.getString(Tag.SOPInstanceUID))
        attributes
    }

}
