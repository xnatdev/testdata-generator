package org.nrg.dicom.process

import static org.dcm4che3.data.Tag.*

class MacroAttributes {

    public static final List<Integer> ISSUER_OF_PATIENT_ID_MACRO_ATTRIBUTES = [
            IssuerOfPatientID,
            IssuerOfPatientIDQualifiersSequence
    ]

    public static final List<Integer> PATIENT_GROUP_MACRO_ATTRIBUTES = [
            SourcePatientGroupIdentificationSequence,
            GroupOfPatientsIdentificationSequence
    ]

}
