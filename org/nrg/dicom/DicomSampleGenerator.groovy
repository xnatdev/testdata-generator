package org.nrg.dicom

import org.dcm4che3.data.Attributes
import org.dcm4che3.data.Sequence
import org.nrg.dicom.process.IODModule
import org.nrg.dicom.process.SOPInstanceReference

import static org.dcm4che3.data.Tag.*
import static org.dcm4che3.data.VR.*

@SuppressWarnings('GrMethodMayBeStatic')
abstract class DicomSampleGenerator {

    protected final Date now = new Date()

    abstract String sampleId()

    abstract void process(String[] args)

    void build(String[] args) {
        final int keyIndex = args.findIndexOf { it in ['-k', '--key'] }
        final List<String> argList = args as List
        argList.remove(keyIndex) // remove flag
        argList.remove(keyIndex) // remove value
        process(argList as String[])
    }

    protected Attributes baseImageFromDefaultInterface(String[] argList) {
        final CliBuilder cli = new CliBuilder()
        cli.with {
            i(longOpt: 'image', 'Image instance from which to build study data.', required: true, args: 1)
        }
        final OptionAccessor params = cli.parse(argList)
        Utils.readDicomFrom(new File("processing/${params.image}"))
    }

    protected void copyDataInModules(Attributes source, Attributes target, IODModule... modules) {
        modules.each { module ->
            module.copyDataFrom(source, target)
        }
    }

    protected void addSampleData(Attributes target, IODModule... modules) {
        modules.each { module ->
            module.addSampleData(target)
        }
    }

    /*
        Supported modules:
            * Patient
            * General Study
            * Patient Study
            * General Series (without Laterality)
            * General Equipment
            * SOP Common
     */
    protected Attributes newStudyAndInstance(String modality, String sopClassUID) {
        final Attributes instance = newBaseInstance(modality, sopClassUID)
        addSampleData(instance, IODModule.PATIENT, IODModule.GENERAL_STUDY, IODModule.PATIENT_STUDY)
        instance
    }

    /*
        Supported modules:
            * General Series (without Laterality)
            * General Equipment
            * SOP Common
     */
    protected Attributes newBaseInstance(String modality, String sopClassUID) {
        final Attributes attributes = new Attributes()
        addSampleData(attributes, IODModule.GENERAL_SERIES, IODModule.GENERAL_EQUIPMENT, IODModule.SOP_COMMON)
        attributes.setString(Modality, CS, modality)
        attributes.setString(SeriesDescription, LO, "${modality} Series")
        attributes.setString(SOPClassUID, UI, sopClassUID)
        attributes
    }

    /*
        Supported modules:
            * SR Document Series
            * General Equipment
            * SOP Common
            * SR Document General
     */
    protected Attributes newStructuredReport(String sopClassUID) {
        final Attributes attributes = new Attributes()
        attributes.setString(SOPClassUID, UI, sopClassUID)
        addSampleData(attributes, IODModule.SR_DOCUMENT_SERIES, IODModule.GENERAL_EQUIPMENT, IODModule.SOP_COMMON, IODModule.SR_DOCUMENT_GENERAL)
        attributes
    }

    protected Attributes createBasicCodedEntry(String codeValue, String codingSchemeDesignator, String codeMeaning) {
        final Attributes entry = new Attributes()
        entry.setString(CodeValue, SH, codeValue)
        entry.setString(CodingSchemeDesignator, SH, codingSchemeDesignator)
        entry.setString(CodeMeaning, LO, codeMeaning)
        entry
    }

    // Add the (0008,1115) Referenced Series Sequence element from PS3.3 C.12.2 Common Instance Reference Module
    protected void addReferencedSeriesSequence(Attributes targetInstance, Attributes... referencedInstances) {
        final Map<String, List<Attributes>> referencedInstancesMap = referencedInstances.groupBy { it.getString(SeriesInstanceUID) }
        final Sequence referencedSeriesSequence = targetInstance.newSequence(ReferencedSeriesSequence, referencedInstancesMap.size())
        referencedInstancesMap.each { seriesInstanceUID, instances ->
            final Attributes referencedSeriesSequenceItem = new Attributes()
            referencedSeriesSequenceItem.setString(SeriesInstanceUID, UI, seriesInstanceUID)
            final Sequence referencedInstanceSequence = referencedSeriesSequenceItem.newSequence(ReferencedInstanceSequence, instances.size())
            instances.each { instance ->
                referencedInstanceSequence << SOPInstanceReference.build(instance)
            }
            referencedSeriesSequence << referencedSeriesSequenceItem
        }
    }

    protected void addContentIdentification(Attributes attributes, String contentLabel, String contentDescription) {
        attributes.setInt(InstanceNumber, IS, 1)
        attributes.setString(ContentLabel, CS, contentLabel.toUpperCase())
        attributes.setString(ContentDescription, LO, contentDescription)
        attributes.setNull(ContentCreatorName, PN)
    }

}
