package org.nrg.dicom

import org.apache.commons.lang3.RandomStringUtils
import org.apache.commons.lang3.RandomUtils
import org.dcm4che3.data.Attributes
import org.dcm4che3.data.UID
import org.dcm4che3.io.DicomInputStream
import org.dcm4che3.io.DicomOutputStream
import org.nrg.dicom.model.Gender
import org.nrg.dicom.model.Person

class Utils {

    private static final List<String> MALE_NAMES = new File('resources/male.txt').readLines()
    private static final List<String> FEMALE_NAMES = new File('resources/female.txt').readLines()
    private static final List<String> LAST_NAMES = new File('resources/surnames.txt').readLines()

    static Attributes readDicomFrom(File input) {
        new DicomInputStream(input).withCloseable { dicomStream ->
            dicomStream.readDataset(-1, -1)
        }
    }

    static void writeDicomToFile(Attributes dataset, File file) {
        new DicomOutputStream(file).withCloseable { dicomStream ->
            dicomStream.writeDataset(dataset.createFileMetaInformation(UID.ExplicitVRLittleEndian), dataset)
        }
    }

    static void writeDicomToFile(Attributes dataset, String fileNameBase) {
        writeDicomToFile(dataset, new File("processing/${fileNameBase}.dcm"))
    }

    static int randomInteger(int maximum) {
        RandomUtils.nextInt(0, maximum)
    }

    static int randomInteger() {
        randomInteger(1000)
    }

    static float randomFloat(BigDecimal minimum, BigDecimal maximum) {
        randomFloat(minimum as float, maximum as float)
    }

    static float randomFloat(float minimum, float maximum) {
        RandomUtils.nextFloat(minimum, maximum).round(2)
    }

    static String randomUppercaseAlphanumeric(int length) {
        RandomStringUtils.randomAlphanumeric(length).toUpperCase()
    }

    static String simpleID() {
        randomUppercaseAlphanumeric(10)
    }

    static<X> X randomItem(List<X> items) {
        items.get(randomInteger(items.size()))
    }

    static Person randomPerson() {
        final Gender gender = RandomUtils.nextBoolean() ? Gender.MALE : Gender.FEMALE
        new Person(gender: gender, firstName: randomItem(gender == Gender.MALE ? MALE_NAMES : FEMALE_NAMES), lastName: randomItem(LAST_NAMES))
    }

    static String randomName() {
        randomPerson().dicomName()
    }

}
