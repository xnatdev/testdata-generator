package org.nrg.dicom.model

enum Gender {

    MALE('M'),
    FEMALE('F')

    String dicomEncoding

    Gender(String encoding) {
        setDicomEncoding(encoding)
    }

}
