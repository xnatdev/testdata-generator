package org.nrg.dicom.model

class Person {

    String firstName
    String lastName
    Gender gender

    String dicomName() {
        "${lastName}^${firstName}"
    }

}
