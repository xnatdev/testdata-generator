import org.nrg.dicom.GeneratorRegistry

@GrabResolver(name = 'dcm4che repo', root = 'https://www.dcm4che.org/maven2', m2Compatible = true)
@Grapes([
        @Grab('org.dcm4che.tool:dcm4che-tool-common:5.15.0'),
        @Grab('org.apache.commons:commons-lang3:3.8.1')
])

final CliBuilder router = new CliBuilder()
router.with {
    k(longOpt: 'key', 'Key identifying desired test data generator.', required: true, args: 1)
}
final OptionAccessor params = router.parse(args)
GeneratorRegistry.byId(params.k as String).build(args)